# Foobartory
Simulation d'une chaîne de production automatique de `foo`, `bar`, `foobar`, et de robots pour les produire.

La simulation commence avec 2 robots, et s'arrête lorsque 30 robots ont été créés.

Commande de lancement: `python foobartory.py`
