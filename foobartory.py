"""
Main script for the foobartory.
Usage: `python foobartory.py`
"""
import datetime

from tools import robot, inventory
from tools import utils

simulation_start = datetime.datetime.now()

my_inventory = inventory.Inventory()
my_robots_pool = robot.RobotsPool()

for i in range(utils.NB_ROBOTS_AT_START):
    new_robot = robot.Robot(my_inventory, my_robots_pool)

for robot_instance in list(my_robots_pool.robots):
    robot_instance.thread.start()

while len(my_robots_pool.robots) < utils.NB_ROBOTS_GOAL:
    pass

for robot_instance in list(my_robots_pool.robots):
    robot_instance.thread.join()

simulation_end = datetime.datetime.now()
simulation_duration = (simulation_end - simulation_start).total_seconds()

print("Simulation ended !")
print(f"Final inventory: {my_inventory}")
print(f"Simulation lasted {simulation_duration} seconds")
