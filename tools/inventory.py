from abc import ABC
import uuid


class Material(ABC):

    def __init__(self):
        self.id = uuid.uuid4()


class Foo(Material):
    pass


class Bar(Material):
    pass


class FooBar(Material):

    selling_price = 1

    def __init__(self, foo: Foo, bar: Bar):
        super().__init__()
        self.id = foo.id, bar.id


class Inventory:

    def __init__(self):
        self.stock_foo = []
        self.stock_bar = []
        self.stock_foobar = []
        self.funds = 0

    def __str__(self):
        return f"Foo: {len(self.stock_foo)} | Bar: {len(self.stock_bar)} | FooBar: {len(self.stock_foobar)} | Funds: {self.funds} €"
