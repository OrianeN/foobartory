import random
import threading
import uuid

from . import builders
from . import inventory
from . import utils

lock_stock_foo = threading.Lock()
lock_stock_bar = threading.Lock()
lock_stock_foobar = threading.Lock()
lock_funds = threading.Lock()
lock_robots_pool = threading.Lock()


class RobotsPool:

    def __init__(self):
        self.robots = set()


class Robot:

    moving_duration = 5

    selling_duration = 10
    max_selling_foobars_once = 5

    new_robot_price_foo = 6
    new_robot_price_funds = 3

    def __init__(self, inventory: inventory.Inventory, robots_pool: RobotsPool):
        self.id = uuid.uuid4()

        self.inventory = inventory
        self.robots_pool = robots_pool
        self.robots_pool.robots.add(self)

        self.thread = RobotThread(self)

        self.last_activity = None

        self.foo_builder = builders.FooBuilder()
        self.bar_builder = builders.BarBuilder()
        self.foobar_builder = builders.FooBarBuilder()

    def move(self):
        utils.time_simulator(self.moving_duration)

    def setup_activity(self, activity):
        if self.last_activity != activity:
            self.move()
        self.last_activity = activity

    def build_foo(self):
        activity = "build_foo"
        self.setup_activity(activity)

        foo = self.foo_builder.build_material()
        if isinstance(foo, inventory.Foo):
            with lock_stock_foo:
                self.inventory.stock_foo.append(foo)

    def build_bar(self):
        activity = "build_bar"
        self.setup_activity(activity)

        bar = self.bar_builder.build_material()
        if isinstance(bar, inventory.Bar):
            with lock_stock_bar:
                self.inventory.stock_bar.append(bar)

    def build_foobar(self):
        activity = "build_foobar"
        self.setup_activity(activity)

        with lock_stock_foo, lock_stock_bar:
            if not self.inventory.stock_foo or not self.inventory.stock_bar:
                return
            else:
                input_foo = self.inventory.stock_foo.pop()
                input_bar = self.inventory.stock_bar.pop()

        foobar = self.foobar_builder.build_material(input_materials=[input_foo, input_bar])
        if foobar:
            with lock_stock_foobar:
                self.inventory.stock_foobar.append(foobar)
        else:
            with lock_stock_bar:
                self.inventory.stock_bar.append(input_bar)

    def sell_foobars(self):
        with lock_stock_foobar:
            if not self.inventory.stock_foobar:
                return

            max_foobar = min(self.max_selling_foobars_once, len(self.inventory.stock_foobar))
            if max_foobar > 1:
                nb_foobars = random.randrange(1, max_foobar + 1)
            else:
                nb_foobars = max_foobar

            with lock_funds:
                for i in range(nb_foobars):
                    self.inventory.stock_foobar.pop()
                    self.inventory.funds += inventory.FooBar.selling_price

        utils.time_simulator(self.selling_duration)

    def buy_robot(self):
        with lock_stock_foo, lock_funds:
            # Check if enough resources available
            if len(self.inventory.stock_foo) < self.new_robot_price_foo or self.inventory.funds < self.new_robot_price_funds:
                return

            # Proceed with payment
            for i in range(self.new_robot_price_foo):
                self.inventory.stock_foo.pop()
            self.inventory.funds -= self.new_robot_price_funds

            # Create new robot
            with lock_robots_pool:
                new_robot = Robot(self.inventory, self.robots_pool)
            new_robot.thread.start()

    def do_activity(self):
        activities = [self.build_foo, self.build_bar, self.build_foobar, self.sell_foobars, self.buy_robot]
        next_activity = random.choice(activities)
        print(f"Next activity by {self.thread.name}: {next_activity}")
        next_activity()
        print(self.inventory)


class RobotThread(threading.Thread):

    def __init__(self, robot):
        threading.Thread.__init__(self)
        self.thread_id = robot.id
        self.robot = robot
        self.robots_pool = self.robot.robots_pool
        self.inventory = self.robot.inventory

    def run(self):
        print("Starting " + self.name)
        while len(self.robots_pool.robots) <= utils.NB_ROBOTS_GOAL:
            self.robot.do_activity()
        print(f"Exiting " + self.name)
