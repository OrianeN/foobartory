import time

NB_ROBOTS_AT_START = 2
NB_ROBOTS_GOAL = 30
TIME_RATE_TO_REAL = 1/100


def time_simulator(seconds):
    """Simulate time spent by a robot, making it wait for a shortened number of seconds"""
    time.sleep(seconds * TIME_RATE_TO_REAL)
