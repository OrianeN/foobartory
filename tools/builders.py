from abc import ABC
import random

from . import inventory
from . import utils


class MaterialBuilder(ABC):
    """Abstract class for building the materials"""

    material_class = inventory.Material

    _build_duration_min = 0
    _build_duration_max = 0

    def build_material(self, input_materials=None):
        utils.time_simulator(self.create_duration)
        return self.material_class()

    @property
    def create_duration(self):
        if self._build_duration_min == self._build_duration_max:
            return self._build_duration_min
        else:
            return random.uniform(self._build_duration_min, self._build_duration_max)


class FooBuilder(MaterialBuilder):
    """Builder for Foo"""
    material_class = inventory.Foo

    _build_duration_min = 1
    _build_duration_max = 1


class BarBuilder(MaterialBuilder):
    """Builder for Bar"""
    material_class = inventory.Bar

    _build_duration_min = 0.5
    _build_duration_max = 2


class FooBarBuilder(MaterialBuilder):
    """Builder for FooBar"""

    material_class = inventory.FooBar

    _build_duration_min = 2
    _build_duration_max = 2

    success_rate = 0.6

    def is_success(self):
        """Returns True if the build operation is to be successful, else False,
        depending on the success rate of the class"""
        failure_rate = 1 - self.success_rate
        return random.choices([True, False], [self.success_rate, failure_rate])[0]

    def build_material(self, input_materials=None):
        success_state = self.is_success()
        if not success_state:
            return None
        else:
            utils.time_simulator(self.create_duration)
            foo = input_materials[0]
            bar = input_materials[1]
            return self.material_class(foo, bar)
